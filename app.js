
/**
 * Module dependencies.
 */

var express 			= require('express')
  , app					= express()
  , http 				= require('http')
  , server				= http.createServer(app)	
  , io					= require('socket.io').listen(server,{log:false})
  , path 				= require('path')
  , matrixDbProvider 	= require('./gridModel').matrixDbProvider;


var matrixDbProvider	= new matrixDbProvider('localhost', 27017,'grid');

// all environments

app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.set('link','http://localhost:3000');
app.use(express.static(path.join(__dirname, 'public')));

app.get('/',function(req,res){
	res.render('index',{link:app.get('link')});
});
var status = function(socket){
	matrixDbProvider.countAll(function(err,all){
	 if (!err){
		 matrixDbProvider.countUnProcessed(function(err,unProcessed){
			 if (!err)
				 socket.emit('status',{all:all,unProcessed:unProcessed});
			 else
				console.log(err);
		 });
	 }
  }); 
};

io.sockets.on('connection', function (socket) {
	socket.on('ready',function(data){
		matrixDbProvider.getMatrixs(1,function(err,data){
			if (!err)
				socket.emit('unProcessedData',data);
		}); 
	});
	
	socket.on('result',function(result){
		matrixDbProvider.updateMatrixs(result.data._id,{'$set':{'result':result.data.result}},function(err,modifiedRecords){
			if (err)
				console.log(err);
		});
		matrixDbProvider.getMatrixs(1,function(err,data){
			if (!err)
				socket.emit('unProcessedData',data);
			else
				console.log(err);
		});
	});
	
	
	socket.on('getStatus',function(data){
		status(socket);
	});
});
server.listen(app.get('port'));
