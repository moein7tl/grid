var Db 			= require('mongodb').Db;
var Connection 	= require('mongodb').Connection;
var Server 		= require('mongodb').Server;
var BSON 		= require('mongodb').BSONPure;
var ObjectID 	= require('mongodb').ObjectID;

var matrixDbProvider = function(host,port,db){
	 this.db= new Db(db, new Server(host, port, {safe: false}, {auto_reconnect: true}, {}));
	 this.db.open(function(){});
};

matrixDbProvider.prototype.matrixCollection = function(callback) {
	this.db.collection('matrix',function(err,matrix){
		if (err){
			callback(err,null);
		} else {
			callback(null,matrix);
		}
	});
};

matrixDbProvider.prototype.getMatrixs = function(limitNum,callback) {
	this.matrixCollection(function(err,matrix){
		if (err){
			callback(err);
		} else {
			matrix.find({result:[]}).limit(limitNum).toArray(function(err,results){
				if (err) callback(err);
				else callback(null,results);
			});
		}
	});
};

matrixDbProvider.prototype.countUnProcessed = function(callback){
	this.matrixCollection(function(err,matrix){
		if (err)
			callback(err);
		else{
			matrix.count({result:[]},function(err,count){
				if (err) callback(err);
				else callback(null,count);
			});
		}
	});
};

matrixDbProvider.prototype.countAll = function(callback){
	this.matrixCollection(function(err,matrix){
		if (err)
			callback(err);
		else{
			matrix.count({},function(err,count){
				if (err) callback(err);
				else callback(null,count);
			});
		}
	});
};

matrixDbProvider.prototype.updateMatrixs = function(id,data,callback) {
	this.matrixCollection(function(err,matrix) {
		if (err)
			callback(err);
		else {
			var o_id = new BSON.ObjectID(id);
			matrix.update({'_id': o_id},data,function(err,modifiedRecords){
				if (err)
					callback(err);
				else
					callback(null,modifiedRecords);
			});
		}
	});
};

exports.matrixDbProvider = matrixDbProvider;