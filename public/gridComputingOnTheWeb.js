var socket   = io.connect('http://localhost:3000');
var GridTask = new Worker('matrix.js');
socket.on('unProcessedData',function(data){
	GridTask.postMessage(data[0]);
	console.log("data:")
	console.log(data[0]);
});
GridTask.onmessage = function(result){
	socket.emit('result',result);
	console.log("result:")
	console.log(result);
};
socket.emit('ready',{});