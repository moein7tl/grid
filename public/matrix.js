var multipliction = function(matrixA,matrixB){
    var result = [];
    for (var i = 0;i < matrixA.length;i++){
        result.push(new Array());
        for (var j = 0;j < matrixB.length;j++)
                result[i].push(0);
    }
    
    for (var i = 0;i < matrixA.length;i++)
        for (var j = 0;j < matrixA[i].length ;j++)
            for(var k = 0;k < matrixB[j].length;k++)
                result[i][j] += matrixA[i][k] * matrixB[k][j];
    return result;
};

var onmessage = function(message){
    
    var result = {
        '_id':message.data._id,
        'result':multipliction(message.data.matrixA,message.data.matrixB)
    };
    postMessage(result);
};