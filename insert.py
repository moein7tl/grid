import random
import pymongo

client = pymongo.MongoClient()
db     = client.grid
matrix = db.matrix

def matrixMaker(d1,d2):
    result = []
    for i in range(d1):
        result.append([])
        for j in range(d2):
            result[i].append(str(random.getrandbits(128)))
    return result;


for i in range(1000):
    rand = random.randint(120,150)
    data = {'matrixA':matrixMaker(rand, rand),
            'matrixB':matrixMaker(rand, rand),
            'result':[],
            }
    matrix.insert(data)
